# Container Scanning Demo Project

## What is this?

A project for testing of GitLab [Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/) functionality.

The CI/CD pipeline builds and pushes a [Docker image with known vulnerabilities](https://github.com/WebGoat/WebGoat) to the project's container registry, then scans it for vulnerabilities using [GitLab Container Scanning](https://docs.gitlab.com/ee/user/application_security/container_scanning/).

This project can be a quick start to demo SAST functionality and expected results or it can be used as a sandbox for testing Container Scanning job modifications.

## Usage

1. Import this project to your SaaS namespace or self-managed instance.
1. Trigger a pipeline.
1. :tada:

## Troubleshooting

If Container Scanning job is failing and it's unclear why, **enable [debug logging](https://docs.gitlab.com/ee/user/application_security/sast/index.html#sast-debug-logging)**

    ```yaml
    variables:
      SECURE_LOG_LEVEL: "debug"
    ```

The build stage requires Docker in Docker, which itself requires a privileged runner. 

To skip the build job and test GitLab Container Scanning functionality on an [existing remote Docker image](https://docs.gitlab.com/ee/user/application_security/container_scanning/#scan-an-image-in-a-remote-registry), use the following example `.gitlab-ci.yml`:

```yaml
include:
  - template: Container-Scanning.gitlab-ci.yml

container_scanning:
  variables:
    DOCKER_IMAGE: "webgoat/goatandwolf:latest" # or another image
    SECURE_LOG_LEVEL: "debug"
  artifacts:
    paths:
      - gl-container-scanning-report.json  
```

You can also [run the GitLab Container Scanning tool as a standalone tool](https://docs.gitlab.com/ee/user/application_security/container_scanning/#running-the-standalone-container-scanning-tool) on your workstation.

## Resources

- [Container Scanning Documentation](https://docs.gitlab.com/ee/user/application_security/container_scanning)
  - [Customizing the container scanning settings](https://docs.gitlab.com/ee/user/application_security/container_scanning/#customizing-the-container-scanning-settings)
  - [Container Scanning CI/CD Variables](https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-cicd-variables)
- [Default CS Job Template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml)
- [Trivy (default GitLab CS engine) upstream](https://github.com/aquasecurity/trivy)
- [Grype (alternative GitLab CS engine) upstream](https://github.com/anchore/grype)

## Contributing

This project is open source (MIT license) and accepting contributions.
